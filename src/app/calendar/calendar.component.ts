import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectDoctorModalComponent } from '../generic/select-doctor-modal/select-doctor-modal.component';
import { CancelAppointmentComponent } from '../generic/cancel-appointment/cancel-appointment.component';

import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { MatDialog } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {
  DrListData, DoctorCalendar, CalendarData, EventData, AvailableDaysRequest,
  SlotAppointment, CalendarRequest, MakeEventRequest, CalendarEvent, availableDaysModel, MemberInfoRequest,
  OpType
} from '../Classes/index';

import { CalendarService } from '../services/calendar.service';
import { LogService } from '../services/log.service';
import { Branch } from '../interfaces/BrnachInterface';

import { CalendarNamingList, HourInterface } from '../interfaces/index';
import { CustomErrorStateMatcher } from '../helpers/CustomErrorStateMatcher';

import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs';

export class MemberInfo {
  Name: string;
  Phone: string;
  Email: string;
}

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
  providers: [
    // The locale would typically be provided on the root module of your application. We do it at
    // the component level here, due to limitations of our example generation script.
    { provide: MAT_DATE_LOCALE, useValue: 'He-il' },

    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})

export class CalendarComponent implements OnInit {
  @ViewChild('calnderFilterForm') FormEl;

  startDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
  endDate = new Date(new Date().getFullYear(), (new Date().getMonth() + 3), new Date().getDate());

  selectedDate: Date;

  drList: DrListData = new DrListData();
  doctorsCalendars: Array<DoctorCalendar> = new Array<DoctorCalendar>();

  availableDaysData: availableDaysModel;
  calendarReq = new CalendarRequest();

  startHour: number = 8;
  endHour: number = 22;
  branch: number = 0;
  calendar: number = 0;
  member: number = 0;
  disableCalendarsSelect: boolean = true;

  descriptionFilter: string = '';
  branchFilter: string = '';
  branchs: Branch[];
  calendars: CalendarNamingList[];
  calendarNamingList: CalendarNamingList[];
  tiketSysId: string = '';
  opType: OpType;
  memberInfo: MemberInfo;

  displayHours: HourInterface[] = [
    { value: 8, viewValue: '08:00' },
    { value: 9, viewValue: '09:00' },
    { value: 10, viewValue: '10:00' },
    { value: 11, viewValue: '11:00' },
    { value: 12, viewValue: '12:00' },
    { value: 13, viewValue: '13:00' },
    { value: 14, viewValue: '14:00' },
    { value: 15, viewValue: '15:00' },
    { value: 16, viewValue: '16:00' },
    { value: 17, viewValue: '17:00' },
    { value: 18, viewValue: '18:00' },
    { value: 19, viewValue: '19:00' },
    { value: 20, viewValue: '20:00' },
    { value: 21, viewValue: '21:00' },
    { value: 22, viewValue: '22:00' }
  ];

  constructor(
    private adapter: DateAdapter<any>,
    private router: Router,
    private calendarService: CalendarService,
    private logService: LogService,
    private spinnerService: Ng4LoadingSpinnerService,
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute) {
    this.branchs = [];
    this.calendars = [];
    this.calendarNamingList = [];
    this.selectedDate = new Date();
    this.selectedDate.setHours(8);
    this.selectedDate.setMinutes(0);
  }

  qBranches = this.calendarService.getBranchList();
  qCalendarNamingList = this.calendarService.calendarGetNameList();
  qAvailableDays = this.calendarService.getCalendarAvailableDays(new AvailableDaysRequest());

  ngOnInit() {

    this.activatedRoute.params.subscribe((params: Params) => {
      this.tiketSysId = params['tiketSysId'];

      if (localStorage.getItem('member') != undefined) {
        this.member = parseInt(localStorage.getItem('member'));
        let memberRequest = new MemberInfoRequest(this.member);
        this.calendarService.memberGetInfo(memberRequest).subscribe(result => {
          if (result.ErrorCode == 0) {
            this.memberInfo = new MemberInfo();
            this.memberInfo.Name = result.MemberInfo.Name;
            this.memberInfo.Phone = result.MemberInfo.CellPhone;
            this.memberInfo.Email = result.MemberInfo.ContactData.EMail;
          }
        });
      }
    });

    this.spinnerService.show();
    this.requestDataFromMultipleSources().subscribe(res => {
      this.branchs = res[0].Entities;
      this.calendarNamingList = res[1].Entities;
      this.availableDaysData = res[2];
      this.spinnerService.hide();
    });
    // const combined = combineLatest(this.qBranches, this.qCalendarNamingList, this.qAvailableDays);

    // combined.subscribe(([branches, calendars, availableDays]) => {
    //   this.spinnerService.hide();
    //   this.branchs = branches.Entities;
    //   this.calendarNamingList = calendars.Entities;
    //   this.availableDaysData = availableDays;
    //   console.log('done');
    // });
  }


  requestDataFromMultipleSources(): Observable<any[]> {
    return forkJoin(
      this.calendarService.getBranchList(),
      this.calendarService.calendarGetNameList(),
      this.calendarService.getCalendarAvailableDays(new AvailableDaysRequest()));
  }

  ///filter the dates in the calendar, only 3 month ahed and by custom dates
  FilterOpenCalanderDays = (d: any): boolean => {
    return this.startDate <= d._d && this.endDate >= d._d && this.filterOpenDates(d._d);
  }

  OnRequestSlotAppointmentEvent(slotAppointment: SlotAppointment) {
    if (slotAppointment.SlotStatus == 1) {
      //let requestHour: string = ((60 / slotAppointment.TotalSlots) * slotAppointment.FromSlot) - (60 / slotAppointment.TotalSlots) + ' : ' + slotAppointment.Hour.toString();

      let requestHour: string = this.parseSlotTime(slotAppointment.TotalSlots , slotAppointment.Hour, slotAppointment.FromSlot);

      const dialogRef = this.dialog.open(SelectDoctorModalComponent, {
        width: '330px',
        data: { title: 'קביעת תור', textBody: '?' + 'האם הינך מעוניין לקבוע תור לשעה ' + requestHour, confirmType: 1 }
      });

      dialogRef.afterClosed().subscribe(result => {
        this.handleConfirmResponse(result, slotAppointment);
      });
    }

    //closed slot logic
    if (slotAppointment.SlotStatus == 2) {
      const dialogRef = this.dialog.open(CancelAppointmentComponent, {
        width: '330px',
        data: { title: 'ביטול תור', textBody: 'האם הינך מעוניין לבטל את התור?', memberName: slotAppointment.MemberName, memberPhone: slotAppointment.MemberPhone, memberEmail: slotAppointment.MemberEmail, confirmType: 1 }
      });

      dialogRef.afterClosed().subscribe(result => {
        this.handleConfirmCancelResponse(result, slotAppointment);
      });
    }
  }

  handleConfirmResponse(response: any, slotAppointment: SlotAppointment) {
    if (response == undefined)
      return void [0];

    //User click yes
    if (response.closeType == 1) {
      this.makeEvent(slotAppointment, 0);
    }

    //User click no
    if (response.closeType == 0) { }
  }

  handleConfirmCancelResponse(response: any, slotAppointment: SlotAppointment) {
    if (response == undefined)
      return void [0];

    ///User click yes
    if (response.closeType == 1) {
      this.makeEvent(slotAppointment, 2);
    }

    ///User click no
    if (response.closeType == 0) { }
  }


  makeEvent(slotAppointment: SlotAppointment, evenType: number) {
    let makeEventRequest = new MakeEventRequest();
    makeEventRequest.OpType = evenType;
    makeEventRequest.CalendarEvent = new CalendarEvent();
    makeEventRequest.CalendarEvent.CalendarId = slotAppointment.CalendarId;
    makeEventRequest.CalendarEvent.EventDate = slotAppointment.EventDate.getDate().toString() + '/' + (slotAppointment.EventDate.getMonth() + 1).toString() + '/' + slotAppointment.EventDate.getFullYear();
    makeEventRequest.CalendarEvent.Description = "בדיקה";
    makeEventRequest.CalendarEvent.EventId = slotAppointment.EventId;
    makeEventRequest.CalendarEvent.Hour = slotAppointment.Hour;
    makeEventRequest.CalendarEvent.FromSlot = slotAppointment.FromSlot;
    makeEventRequest.CalendarEvent.ToSlot = slotAppointment.ToSlot;
    makeEventRequest.CalendarEvent.MemberSysId = 137635;//this.member
    makeEventRequest.CalendarEvent.TiketSysId = this.tiketSysId != '' ? parseInt(this.tiketSysId) : 0;

    this.spinnerService.show();
    this.calendarService.makeEvent(makeEventRequest).subscribe(response => {
      this.spinnerService.hide();
      if (response.ErrorCode > 0) {
        this.logService.error(response.ErrorMessage);
      } else {
        this.logService.success('עדכון בוצע בהצלחה');
        this.router.navigate(['/thankyou']);
        //this.getCalendars();
      }
    });
  }

  matInputChange(event: any) {
    this.calendarReq.RequestDay = event.value.date().toString() + '/' + (event.value.month() + 1).toString() + '/' + event.value.year().toString();
    this.getCalendars();
  }

  getCalendars() {
    this.spinnerService.show();

    this.calendarReq.FromDate = this.startDate;
    this.calendarReq.ToDate = this.endDate;
    this.calendarReq.FromHour = this.startHour <= 0 ? 6 : this.startHour;
    this.calendarReq.ToHour = this.endHour <= 0 ? 23 : this.endHour;
    this.calendarReq.BranchSysId = this.branch;
    this.calendarReq.CalendarSysId = this.calendar;

    this.calendarService.getCalendars(this.calendarReq).subscribe(result => {
      this.spinnerService.hide();
      if (result.ErrorCode == 0) {
        //this.disableSelect = false;
        this.doctorsCalendars = new Array<DoctorCalendar>();
        for (let calendar of result.Calendars) {
          this.doctorsCalendars.push(this.filterCalendars(calendar, result.Events));
        }
      }
    });
  }

  matcher = new CustomErrorStateMatcher();

  calanderFilterChange(form: NgForm): void {
    ///validate the time
    if (!(this.startHour <= 0 || this.endHour <= 0 || (this.startHour <= this.endHour)))
      return void [0];

    this.disableCalendarsSelect = !(this.branch > 0);
    if (true) {
      let cnl = Object.assign(new Array<CalendarNamingList>(), this.calendarNamingList);
      this.calendars = cnl.filter(c => parseInt(c.Code) === this.branch);
    }

    ///clear the data b-4 getting new data!!
    this.doctorsCalendars = new Array<DoctorCalendar>();
    ///clear the selected date to gate new data
    this.selectedDate = null;

    this.spinnerService.show();
    let availableDaysRequest = new AvailableDaysRequest();
    availableDaysRequest.FromDate = this.startDate.getDate().toString() + '/' + (this.startDate.getMonth() + 1).toString() + '/' + this.startDate.getFullYear();
    availableDaysRequest.ToDate = this.endDate.getDate().toString() + '/' + (this.endDate.getMonth() + 1).toString() + '/' + this.endDate.getFullYear();
    availableDaysRequest.FromHour = this.startHour <= 0 ? 6 : this.startHour;
    availableDaysRequest.ToHour = this.endHour <= 0 ? 23 : this.endHour;

    ///TODO => add the 'branch' and 'calendar' SysId to the request
    availableDaysRequest.BranchSysId = this.branch;
    availableDaysRequest.CalendarSysId = this.calendar;

    this.calendarService.getCalendarAvailableDays(availableDaysRequest).subscribe(availableDays => {
      this.spinnerService.hide();
      this.availableDaysData = availableDays;
    });
  }

  private filterOpenDates(date: Date): boolean {
    const _m = date.getMonth() + 1;
    const _d = date.getDate();

    const om = this.availableDaysData.Months.filter(m => m.Month === _m);
    if (om == null || om.length <= 0) {
      return false;
    }
    const od = om[0].Days.filter(d => d == _d);
    if (od !== null && od.length > 0) {
      return true;
    }
    return false;
  }

  private filterCalendars(calendar: CalendarData, events: Array<EventData>): DoctorCalendar {
    let doctorCalendar: DoctorCalendar = new DoctorCalendar();
    doctorCalendar.calendarData = Object.assign(new CalendarData, calendar);
    doctorCalendar.eventDataList = events.filter(e => e.CalendarId === calendar.CalendarId);
    return doctorCalendar;
  }

  onSubmit() { }

  private resetHours() {
    this.startHour = 8;
    this.endHour = 22;
    this.branch = 0;
    this.calendar = 0;
    this.disableCalendarsSelect = true;
    this.selectedDate = null;

    this.doctorsCalendars = new Array<DoctorCalendar>();
  }

  private parseSlotTime(slots: number, startH: number, slotNumber: number): string {
    let mStart = 60 / slots;

    return this.padTime(startH) + ':' + this.padTime((mStart * (slotNumber - 1)));
  }
  private padTime(val: number): string {
    return val > 9 ? val.toString() : ('0' + val.toString());
  }

}