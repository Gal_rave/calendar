import { Pipe, PipeTransform } from '@angular/core';
  
@Pipe({  
    name: 'descriptionFilter',  
    pure: false  
})  
  
export class DoctorDescriptionFilter implements PipeTransform {  
    transform(items: any[], filter: any): any {  
        if (!items || !filter) {  
            return items;  
        }  
        return items.filter(item => item.calendarData.Description.indexOf(filter) !== -1);  
    }  
}  