import { Pipe, PipeTransform } from '@angular/core';
  
@Pipe({  
    name: 'brnachFilter',  
    pure: false  
})  
  
export class BranchNameFilter implements PipeTransform {  
    transform(items: any[], filter: any): any {  
        if (!items || !filter) {  
            return items;  
        }  
        return items.filter(item => item.calendarData.BranchName.indexOf(filter) !== -1);  
    }  
}  