import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class CustomErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const startHour = form.form.value.startHourIntake;
    const endHour = form.form.value.endHourIntake;
    return !(startHour <= 0 || endHour <= 0 || (startHour <= endHour));
  }
}