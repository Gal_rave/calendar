import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';
import {tap} from "rxjs/operators";
import { Router } from '@angular/router';
import { LogService } from '../services/log.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    currentThis: any;
    constructor(private router: Router, 
        private logService: LogService) {
        this.currentThis = this;
    } 

    intercept(request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {
        this.currentThis = this;
        let sessionKey: any =  localStorage.getItem('sessionKey');
        //let currentUser = localStorage.getItem('currentUser');
        if(sessionKey == undefined){
            this.router.navigate(['/login'], { queryParams: { vld: 0 }});
        } else {

            const authReq = request.clone({
                headers: request.headers
                .set('sessionKey', sessionKey)
                .set('Content-Type', 'application/json')
              });

            return next.handle(authReq);
            // let authReq = request.clone({
            //     headers: request.headers
            //     .set('X-Auth-Token', currentUser)
            //     .set('Content-Type', 'application/json')
            //   });
            // return next.handle(authReq).do((event: HttpEvent<any>) => {
            //     if(event instanceof HttpResponse){
            //         //do stuff here
            //     }
            // }, (err: any) => {
            //     if(err instanceof HttpErrorResponse) {
            //         if(err.status === 401){
            //             //this.logService.toastr.dispose();
            //             this.router.navigate(['/login'], { queryParams: { vld: 0 }});
            //         }
            //     }
            // });

        }
        

    }
}