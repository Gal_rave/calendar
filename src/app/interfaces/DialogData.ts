export interface DialogData {
    title: string;
    textBody: string;
    confirmType: number;
  }