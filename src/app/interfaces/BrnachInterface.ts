export interface Branch{
    SysId: number;
    Code: string;
    Description: string;
}