export interface CalendarNamingList{
    SysId: number;
    Code: string;
    Description: string;
}