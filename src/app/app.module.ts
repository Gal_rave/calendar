import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { routing } from './app.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResourceService } from './services/resource.service';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { MatBadgeModule,MatBottomSheetModule,MatButtonModule,MatButtonToggleModule,MatCardModule,
  MatCheckboxModule,MatChipsModule,MatDatepickerModule,MatDialogModule,MatDividerModule,
  MatExpansionModule,MatGridListModule,MatIconModule,MatInputModule,MatListModule,
  MatMenuModule,MatNativeDateModule,MatPaginatorModule,MatProgressBarModule,MatProgressSpinnerModule,
  MatRadioModule,MatRippleModule,MatSelectModule,MatSidenavModule,MatSliderModule,
  MatSlideToggleModule,MatSnackBarModule,MatSortModule,MatStepperModule,MatTableModule,
  MatTabsModule,MatToolbarModule,MatTooltipModule,MatTreeModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SelectDoctorModalComponent } from './generic/select-doctor-modal/select-doctor-modal.component';
import { HomeComponent } from './home/home.component';
import { CalendarComponent } from './calendar/calendar.component';
import { LogService } from './services/log.service';
import { WorkingHoursComponent } from './generic/working-hours-component/working-hours-component';
import { AutologinComponent } from './autologin/autologin.component';
import { DoctorDescriptionFilter } from './DoctorDescriptionFilter';
import { BranchNameFilter } from './BranchNameFilter';
import { CancelAppointmentComponent } from './generic/cancel-appointment/cancel-appointment.component';
import { ThankyouComponent } from './thankyou/thankyou.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SelectDoctorModalComponent,
    HomeComponent,
    CalendarComponent,
    WorkingHoursComponent,
    AutologinComponent,
    DoctorDescriptionFilter,
    BranchNameFilter,
    CancelAppointmentComponent,
    ThankyouComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    BrowserAnimationsModule,
    Ng4LoadingSpinnerModule.forRoot(),
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule
  ],
  providers: [
    ResourceService,
    {
      provide: APP_INITIALIZER,
      useFactory: (config: ResourceService) => () => config.getApiUrl(),
      deps: [ResourceService],
      multi: true
    },
    LogService
  ],
  entryComponents: [SelectDoctorModalComponent, CancelAppointmentComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
