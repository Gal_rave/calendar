import { OnInit, AfterViewChecked,Component, Input, OnChanges, EventEmitter, Output, SimpleChange, ComponentFactoryResolver} from "@angular/core";
import { DoctorCalendar } from '../../Classes/DoctorCalendar';
import { SlotAppointment } from '../../Classes/SlotAppointment';
import { EventData } from '../../Classes/EventData';
import { CalanderModel } from '../../Classes/CalanderModel';
import { WorkingHours } from '../../Classes/CalanderModel';
import { Slot } from '../../Classes/CalanderModel';
import {TooltipPosition} from '@angular/material';
import {FormControl} from '@angular/forms';

@Component({
    selector: 'working-hours',
    templateUrl: './working-hours-component.html',
    styleUrls: ['./working-hours-component.css'],
    //outputs: ['onNewEntryAdded']
  })
export class WorkingHoursComponent implements OnInit,AfterViewChecked{
    positionOptions: TooltipPosition[] = ['above'];
    position = new FormControl('above');

    @Input() doctorCalendar: DoctorCalendar;
    @Input() selectedDate: Date;

    @Output() RequestSlotAppointmentEvent: EventEmitter<SlotAppointment> = new EventEmitter<SlotAppointment>();

    calanderModel: CalanderModel = new CalanderModel();

    constructor(){}
    public ngAfterViewChecked():void{}

    ngOnInit(){
        this.parseworkingHours(this.doctorCalendar.calendarData.StartHour,this.doctorCalendar.calendarData.EndHour,
            this.doctorCalendar.calendarData.HourSlots, this.doctorCalendar.eventDataList);
    }

    public OnRequestSlotAppointment(workingHours: WorkingHours, slot: Slot) {
        //if(!slot.openSlot)///if slot isent open return
            //return void[0];
        let slotAppointment = new SlotAppointment();
        slotAppointment.CalendarId = this.doctorCalendar.calendarData.CalendarId;
        slotAppointment.EventDate = this.selectedDate;
        slotAppointment.FromSlot = slot.slotNumber;
        slotAppointment.ToSlot = slot.slotNumber;
        slotAppointment.Hour = workingHours.start;
        slotAppointment.TotalSlots = this.doctorCalendar.calendarData.HourSlots;
        slotAppointment.MemberName = slot.memberName;
        slotAppointment.MemberPhone = slot.memberPhone;
        slotAppointment.MemberEmail = slot.memberEmail;
        //if the slot is open then the slot status is 1 else it is closed and it is one
        slotAppointment.SlotStatus = slot.openSlot == true ? 1: 2;
        slotAppointment.EventId = slot.eventId;
        this.RequestSlotAppointmentEvent.emit(slotAppointment);
      }

    private parseworkingHours(startH: number, endH: number, slots:number, eventsList: Array<EventData>):void{
        let i = startH
        for(;i<endH;i++){
            let workingHour = new WorkingHours(i, (i+1));
            
            for(let si = 1; si <= slots; si++){
                const event = eventsList.filter(e => e.Hour >= workingHour.start && e.Hour < workingHour.end && (e.FromSlot === si || (e.FromSlot < si && e.ToSlot >= si)));                
                if(event.length > 0){
                    console.log('event', event.length, event);
                }
                let slot = new Slot(si, (event.length > 0), event.length > 0 ? event[0].MemberName: '', 
                                    event.length > 0 ? event[0].MemberPhone: '', event.length > 0 ? event[0].MemberEmail: '', 
                                    event.length > 0 ? event[0].EventId : 0, this.parseSlotTime(slots, workingHour.start, si));

                workingHour.slots.push(slot);
            }
            this.calanderModel.workingHours.push(workingHour);
        }
    }

    private parseSlotTime(slots:number, startH: number, slotNumber: number):string{
        let mStart = 60 / slots;
        
        return this.padTime(startH) + ':' + this.padTime((mStart * (slotNumber - 1)));
    }
    private padTime(val:number):string{
        return val > 9? val.toString() : ('0' + val.toString());
    }
}