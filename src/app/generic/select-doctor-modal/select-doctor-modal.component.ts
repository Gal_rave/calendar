import {Component, Inject, OnInit, Output, EventEmitter } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogData } from '../../interfaces/index';
import {DialogCloseModel} from '../../Classes/index';

@Component({
  selector: 'app-select-doctor-modal',
  templateUrl: './select-doctor-modal.component.html',
  styleUrls: ['./select-doctor-modal.component.css']
})
export class SelectDoctorModalComponent implements OnInit {

  @Output() actionPressed: EventEmitter<any> = new EventEmitter<any>();
  constructor(public dialogRef: MatDialogRef<SelectDoctorModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ngOnInit() {
  }

  onNoClick(): void {
    let response = Object.assign(new DialogCloseModel,  { closeType: 0, confirmType: this.data.confirmType });    
    this.actionPressed.next(response);
    this.dialogRef.close(response);
  }

  onYesClick(){
    let response = Object.assign(new DialogCloseModel,  { closeType: 1, confirmType: this.data.confirmType });    
    this.actionPressed.next(response);
    this.dialogRef.close(response);
  }
}
