import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectDoctorModalComponent } from './select-doctor-modal.component';

describe('SelectDoctorModalComponent', () => {
  let component: SelectDoctorModalComponent;
  let fixture: ComponentFixture<SelectDoctorModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectDoctorModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectDoctorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
