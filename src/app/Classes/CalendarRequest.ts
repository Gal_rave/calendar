export class CalendarRequest {
    RequestDay: string;
    SessionKey: string;
    public FromDate: Date;
    public ToDate: Date;
    public FromHour: number;
    public ToHour: number;
    public BranchSysId: number;
    public CalendarSysId: number;
}   