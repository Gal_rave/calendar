export class AvailableDaysRequest {
    constructor() {
        let startDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        let endDate = new Date(new Date().getFullYear(), (new Date().getMonth() + 3), new Date().getDate());
        this.FromDate = startDate.getDate().toString() + '/' + (startDate.getMonth() + 1).toString() + '/' + startDate.getFullYear();
        this.ToDate = endDate.getDate().toString() + '/' + (endDate.getMonth() + 1).toString() + '/' + endDate.getFullYear();
        this.FromHour = 6;
        this.ToHour = 23;
        this.SessionKey = '';
    }
    public FromDate: string;
    public ToDate: string;
    public FromHour: number;
    public ToHour: number;
    public SessionKey: string;
    public BranchSysId: number;
    public CalendarSysId: number;
}