export class CalendarData {
    public AccountSysId: number;
    public CalendarId: number;
    public EndHour: number;
    public HourSlots: number;
    public ObjectType: number;
    public OwnerSysId: number;
    public StartHour: number;
    public SysId: number;

    public Description: string;
    public BranchName: string
    public CreatedDate: Date;
    public UpdateDate: Date;

    public IsManualSysID: boolean;
}