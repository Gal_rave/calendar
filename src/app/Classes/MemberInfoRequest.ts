export class MemberInfoRequest {
    constructor(memberSysId: number) {
        this.SessionKey = '';
        this.MemberSysId = memberSysId;
    }
    public SessionKey: string;
    public MemberSysId: number;
}