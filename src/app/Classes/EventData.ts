export class EventData {
    public CalendarId: number;
    public EventId: number;
    public FromSlot: number;
    public Hour: number;
    public MemberSysId: number;
    public MemberEmail: string;
    public MemberName: string;
    public MemberPhone: string;
    public ObjectType: number;
    public OwnerSysId: number;
    public SysId: number;
    public ToSlot: number;

    public Description: string;

    public CreatedDate: Date;
    public EventDate: Date;
    public UpdateDate: Date;

    public IsManualSysID: boolean;
}