export class MonthModel{
    public Month: number;
    public Days:Array<number> = new Array<number>();
}

export class availableDaysModel{
    public ErrorCode:number;
    public ErrorMessage: string;
    public Reference: string;
    public Months: Array<MonthModel> = new Array<MonthModel>();
}