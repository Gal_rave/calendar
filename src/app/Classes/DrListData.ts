import { EventData } from './EventData';
import { CalendarData } from './CalendarData';

export class DrListData {
    public Calendars: Array<CalendarData>;// = new Array<CalendarData>();
    public ErrorCode: number;
    public ErrorMessage: string;
    public Reference: string;
    public Events: Array<EventData>;// = new Array<EventData>();
}