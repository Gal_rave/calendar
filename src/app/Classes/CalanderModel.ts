export class Slot{
    constructor(n:number, open:boolean = false, memberName: string = '', memberPhone: string = '', memberEmail: string ='', eventId: number = 0, timing: string = ''){
        this.slotNumber = n;
        this.openSlot = !open;
        this.memberName = memberName;
        this.memberPhone = memberPhone;
        this.memberEmail = memberEmail;
        this.eventId = eventId;
        this.slotTimes = timing;
    }

    public slotNumber:number;
    public openSlot:boolean = true;
    public memberName: string;
    public memberPhone: string;
    public memberEmail: string; 
    public eventId: number;
    public slotTimes: string; 
}

export class WorkingHours{
    constructor(s:number, e: number){
        this.start = s;
        this.end = e;
    }

    public start: number;
    public end: number;
    public slots: Array<Slot> = new Array<Slot>();
    public slotsClass:string = 'slots-length-';
}

export class CalanderModel{
    public workingHours: Array<WorkingHours> = new Array<WorkingHours>();
}