export class CalendarEvent {
    EventId: number;
    CalendarId: number;
    MemberSysId: number;
    Hour: number;
    FromSlot: number;
    ToSlot: number;
    EventDate: string;
    Description: string;
    TiketSysId: number;
}