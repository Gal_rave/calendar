export class SlotAppointment{
    public CalendarId: number;
    public FromSlot: number;
    public Hour: number;
    public ToSlot: number;
    public EventDate: Date;
    public TotalSlots: number;
    public SlotStatus: number;
    public MemberName: string;
    public MemberPhone: string;
    public MemberEmail: string;
    public EventId: number;
}