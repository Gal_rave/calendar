import { CalendarData } from './CalendarData';
import { EventData } from './EventData';

export class DoctorCalendar{
    public calendarData: CalendarData;
    public eventDataList: Array<EventData> = new Array<EventData>();
}