import { CalendarEvent } from './CalendarEvent';

export class MakeEventRequest{
    OpType: number;//at the moment it 0 for create event, in the future it will serve other options
    CalendarEvent: CalendarEvent;
    SessionKey: string;
}