import { Component, OnInit } from '@angular/core';
import { AuthenticationModel } from '../Classes/AuthenticationModel';
import { AuthenticationService } from '../services/authentication.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  authenticationModel = new AuthenticationModel('', '');

  constructor(private authService: AuthenticationService,
    private spinnerService: Ng4LoadingSpinnerService,
    private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.spinnerService.show();
    this.authService.login(this.authenticationModel).subscribe(result => {
      this.spinnerService.hide();
      if (result.ErrorCode == 0) {
        if (result.SessionKey != '') {
          localStorage.setItem('currentUser', result.SessionKey);
          this.router.navigate(['/calendar']);
        }
      }
    });
  }

}
