import { Injectable, InjectionToken } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
//import { map, filter, switchMap } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

export const APP_INITIALIZER = new InjectionToken<Array<() => void>>('Application Initializer');

@Injectable({
  providedIn: 'root'
})
export class ResourceService {
  private appConfig: any

  constructor(private http: HttpClient) { }

  // public getApiUrl(): Observable<any> {
  //   return this.http.get("../assets/config.json");
  // }

  getApiUrl(){
    return this.http.get('../assets/config.json').pipe(
      map((res) => { 
        this.appConfig = res;
        console.log('resource service', res);
       }));
    

    // var promise = this.http.get('../assets/config.json').map(res => res.json()).toPromise();
    //     promise.then(config => this.appConfig = config);
    //     return promise;

    //return this.http.get('../assets/config.json').pipe(map(response => { return response;}))
  }
}
