import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {
  MemberInfoRequest, CalendarRequest,
  AvailableDaysRequest, MakeEventRequest
} from '../Classes/index';
import { ResourceService } from './resource.service';
import { ErrorService } from './error.service';
import * as data from '../../assets/config.json';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  apiUrl: any;

  constructor(private http: HttpClient,
    private resourceSerivce: ResourceService,
    private errorService: ErrorService) {
    this.apiUrl = data;
    // this.apiUrl = this.resourceSerivce.getApiUrl();
    // console.log('const', data);
  }

  getCalendarAvailableDays(availableDaysRequest: AvailableDaysRequest): Observable<any> {
    availableDaysRequest.SessionKey = localStorage.getItem('currentUser');
    return this.http.post<any>(this.apiUrl.BASE_URL + '/CalendarGetAvailableDays.aspx', availableDaysRequest).pipe(catchError(this.errorService.HnadleError<any>('Error getting available days')));
  }

  getCalendars(calendarReq: CalendarRequest): Observable<any> {
    calendarReq.SessionKey = localStorage.getItem('currentUser');
    return this.http.post<any>(this.apiUrl.BASE_URL + '/CalendarGetList.aspx', calendarReq).pipe(catchError(this.errorService.HnadleError<any>('Error getting calendars')));
  }

  makeEvent(makeEventRequest: MakeEventRequest): Observable<any> {
    makeEventRequest.SessionKey = localStorage.getItem('currentUser');
    return this.http.post<any>(this.apiUrl.BASE_URL + '/CalendarMakeEvent.aspx', makeEventRequest).pipe(catchError(this.errorService.HnadleError<any>('Error getting calendars')));
  }

  getBranchList(): Observable<any> {
    return this.http.post<any>(this.apiUrl.BASE_URL + '/BranchGetList.aspx', { "SessionKey": localStorage.getItem('currentUser') }).pipe(catchError(this.errorService.HnadleError<any>('Error getting calendars')));
  }

  calendarGetNameList(): Observable<any> {
    return this.http.post<any>(this.apiUrl.BASE_URL + '/CalendarGetNameList.aspx', { "SessionKey": localStorage.getItem('currentUser') }).pipe(catchError(this.errorService.HnadleError<any>('Error getting calendars')));
  }

  memberGetInfo(MemberInfoRequest: MemberInfoRequest): Observable<any> {
    MemberInfoRequest.SessionKey = localStorage.getItem('currentUser');
    return this.http.post<any>(this.apiUrl.BASE_URL + '/MemberGetInfo.aspx', MemberInfoRequest).pipe(catchError(this.errorService.HnadleError<any>('Error getting calendars')));
  }
}
