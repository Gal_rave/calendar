import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private toastr: ToastrService) { }

  error(message: string, title?: string) {
    title = title ? title : 'Error';
    this.toastr.error(message, title);
  }

  warning(message: string, title?: string) {
    title = title ? title : 'Warning';
    this.toastr.warning(message, title);
  }

  success(message: string, title?: string) {
    title = title ? title : 'Success';
    this.toastr.success(message, title);
  }
}
