import { Injectable } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { LogService } from './log.service';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(
    private spinnerService: Ng4LoadingSpinnerService,
    private logService: LogService) { }

    HnadleError<T>(operation = 'operation', result?: T){
      return (error: any): Observable<T> => {
        this.spinnerService.hide();
        console.log(error);

        return of(result as T);
      };
    }
}
