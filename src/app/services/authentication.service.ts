import { Injectable, InjectionToken } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as data from '../../assets/config.json';
import { ErrorService } from './error.service';
import { catchError } from 'rxjs/operators';
import { AuthenticationModel } from '../Classes/AuthenticationModel';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  apiUrl: any;
  constructor(
    private http: HttpClient,
    private errorService: ErrorService
  ) { 
    this.apiUrl = data;
  }

  login(request: AuthenticationModel): any{
    return this.http.post<any>(this.apiUrl.BASE_URL + '/SystemLogin.aspx', request).pipe( catchError(this.errorService.HnadleError<any>('Error login to the system')) );
  }

  loginSessionKey(sessionKey: string): any{
    return this.http.post<any>(this.apiUrl.BASE_URL + '/SystemLogin.aspx', sessionKey).pipe( catchError(this.errorService.HnadleError<any>('Error login to the system')) );
  }

  logout(){
    localStorage.removeItem('currentUser');
  }
}
