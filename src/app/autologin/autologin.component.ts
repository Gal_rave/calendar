import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as data from '../../assets/config.json';

@Component({
  selector: 'app-autologin',
  templateUrl: './autologin.component.html',
  styleUrls: ['./autologin.component.css']
})
export class AutologinComponent implements OnInit {
  sessionKey: string = '';
  memberId: string = '';
  tiketSysId: string = '';
  apiUrl: any;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router) {
      this.apiUrl = data;
     }

  ngOnInit() {

    window.addEventListener("message", () => {
      this.receiveMessage();
    }, false)

    // this.activatedRoute.params.subscribe((params: Params) => {
    //   this.sessionKey = params['sessionkey'];
    //   this.memberId = params['memberId'];
    //   this.tiketSysId = params['tiketSysId'];
    //   if (this.sessionKey != '' && this.memberId != '') {
    //     this.router.navigate(['/calendar']);
    //     localStorage.removeItem('currentUser');

    //     localStorage.setItem('currentUser', this.sessionKey);
    //     localStorage.setItem('member', this.memberId);
    //     this.router.navigate(['/calendar/' + this.tiketSysId]);
    //   }
    // });
  }

  receiveMessage: any = (event: any) => {
    console.log('message recived', event);
  }

  sendMessage() {
    console.log(this.apiUrl.IFRAME_URL);
    console.log('send message url', this.apiUrl.IFRAME_URL);
    window.postMessage("test message data", this.apiUrl.IFRAME_URL);
  }

}
