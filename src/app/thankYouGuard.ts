import { CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { ThankyouComponent } from './thankyou/thankyou.component';

@Injectable()
export class SaveFormsGuard implements CanDeactivate<ThankyouComponent> {

  canDeactivate(component: ThankyouComponent) {
    return false;
  }

}