import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CalendarComponent } from './calendar/calendar.component';
import { AutologinComponent } from './autologin/autologin.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { SaveFormsGuard } from './thankYouGuard';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'autoLogin', component: AutologinComponent },
    { path: 'calendar/:tiketSysId', component: CalendarComponent },
    { path: 'autoLogin/:sessionkey/:memberId/:tiketSysId', component: AutologinComponent },
    { path: 'login', component: LoginComponent },
    { path: 'thankyou', component: ThankyouComponent, canDeactivate:[SaveFormsGuard] },
    
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing:ModuleWithProviders = RouterModule.forRoot(appRoutes, { useHash: true });